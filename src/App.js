import { ReactCreateElem } from "./components/ReactCreateElem";
import { ReactComponent } from './components/ReactComponent';
import { ReactPureComponent } from "./components/ReactPureComponent";
import { FunctionalComponent } from "./components/FunctionalComponent";
import { Card, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <Row>
      <Col xs="6">
        <h1>Hello, React!</h1>
        <Card >
          <Card.Body>
            <Card.Title>This is <b>ReactCreateElem component</b> </Card.Title>
            <ReactCreateElem />
          </Card.Body>
        </Card>
        <div className="mb"></div>
        <ReactComponent />
        <div className="mb"></div>
        <ReactPureComponent />
        <div className="mb"></div>
        <FunctionalComponent />
      </Col>
    </Row>
  );
}

export default App;
