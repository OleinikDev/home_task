import React, { useState, useRef } from 'react';
import { Card, Button, InputGroup, FormControl, Form } from 'react-bootstrap';

export const FunctionalComponent = () => {
    const input = useRef();
    const [count, setCount] = useState(0);
    const [value, setValue] = useState("");
    const [show, setShow] = useState(false);
    const [activate, setActivate] = useState(true);

    function increment() {
        setCount(prevState => ++prevState);
    }

    function decrement() {
        setCount(prevState => --prevState);
    }

    function showRes() {
        setValue(input.current.value)
        setShow(true);
    }

    return (
        <Card>
            <Card.Body>
                <Card.Title>This is <b>FunctionalComponent</b></Card.Title>
                <div className="functionalSearch mb-4">
                    <InputGroup>
                        <FormControl
                            onChange={e => setShow(false)}
                            ref={input}
                        />
                        <Button
                            variant="outline-secondary"
                            onClick={() => showRes()}
                        >
                            Искать
                        </Button>
                    </InputGroup>
                    <p>{show && value !== "" ? `Вы ищите ${value}` : null}</p>
                </div>
                <div className="switcher">
                    <Form.Check
                        type="switch"
                        label="Activate counter"
                        onChange={() => setActivate(!activate)}
                    />
                </div>
                <div className="functionalCounter">
                    <p>Counter: {count}</p>
                    <Button
                        onClick={() => increment()}
                        variant="primary"
                        className="mr-2"
                        disabled={activate}
                    >
                        Increment
                    </Button>
                    <Button
                        onClick={() => decrement()}
                        variant="danger"
                        disabled={activate}
                    >
                        Decrement
                    </Button>
                </div>
            </Card.Body>
        </Card>
    )
}
