import React, { Component } from 'react';
import { Card, Button, InputGroup, FormControl, Form } from 'react-bootstrap';

export class ReactComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            show: false,
            activate: true
        }
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
        this.showRes = this.showRes.bind(this);

        this.input = React.createRef();
    }


    increment() {
        this.setState({ count: this.state.count + 1 })
    }

    decrement() {
        this.setState({ count: this.state.count - 1 })
    }

    showRes() {
        this.setState({ show: true });
    }
    render() {
        return (
            <Card>
                <Card.Body>
                    <Card.Title>This is <b>ReactComponent</b> </Card.Title>
                    <div className="functionalSearch mb-4">
                        <InputGroup>
                            <FormControl
                                onChange={() => this.setState({ show: false })}
                                ref={this.input}
                            />
                            <Button
                                variant="outline-secondary"
                                onClick={this.showRes}
                            >
                                Искать
                            </Button>
                        </InputGroup>
                        <p>{this.state.show && this.input.current.value !== "" ? `Вы ищите ${this.input.current.value}` : null}</p>
                    </div>
                    <div className="switcher">
                        <Form.Check
                            type="switch"
                            label="Activate counter"
                            onChange={() => this.setState({ activate: !this.state.activate })}
                        />
                    </div>
                    <div className="componentCounter">
                        <p>Counter: {this.state.count}</p>
                        <Button
                            onClick={this.increment}
                            variant="primary"
                            className="mr-2"
                            disabled={this.state.activate}
                        >
                            Increment
                        </Button>
                        <Button
                            onClick={this.decrement}
                            variant="danger"
                            disabled={this.state.activate}
                        >
                            Decrement
                        </Button>
                    </div>
                </Card.Body>
            </Card>
        )
    }
}
