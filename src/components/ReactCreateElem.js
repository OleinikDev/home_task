import React, { Component } from 'react';

export class ReactCreateElem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
        this.increment = this.increment.bind(this);
        this.decrement = this.decrement.bind(this);
    }

    increment() {
        this.setState({ count: this.state.count + 1 })
    }

    decrement() {
        this.setState({ count: this.state.count - 1 })
    }

    render() {
        const h1 = React.createElement(
            'p',
            { id: "createElemHeader" },
            `Counter: ${this.state.count}`
        )
        const incBtn = React.createElement(
            'button',
            { id: "createElemInc", class: "btn btn-primary mr-2", onClick: () => this.increment() },
            "Increment"
        )
        const decBtn = React.createElement(
            'button',
            { id: "createElemDec", class: "btn btn-danger", onClick: () => this.decrement() },
            "Decrement"
        )
        return (
            React.createElement("div", null, h1, incBtn, decBtn)
        )
    }
}
